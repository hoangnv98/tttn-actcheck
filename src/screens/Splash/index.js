import React, {Component} from 'react';
import {View, Animated, StyleSheet, ImageBackground} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import {Images} from '@config';
import {
  ScreenWidth,
  ScreenHeight,
  getHeight,
  normalize,
  fontFamily,
} from '@common';

export class Splash extends Component {
  constructor(props) {
    super(props);

    this._opacityImage = new Animated.Value(0);
    this._moveText = new Animated.Value(0);
  }

  componentDidMount() {
    let timer = setInterval(() => {
      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'MainStack'}],
        }),
      );
      clearInterval(timer);
    }, 3000);
    Animated.parallel([
      Animated.timing(this._opacityImage, {
        toValue: 1,
        duration: 1500,
        useNativeDriver: true,
      }),
      Animated.timing(this._moveText, {
        toValue: -20,
        duration: 600,
        useNativeDriver: true,
      }),
    ]).start();
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={Images.bg_splash} style={styles.bg_image}>
          <View style={styles.content}>
            <Animated.Image
              source={Images.logo}
              style={[
                styles.logo,
                {
                  opacity: this._opacityImage,
                },
              ]}
            />
            <Animated.Text
              style={[
                styles.title,
                {transform: [{translateY: this._moveText}]},
              ]}>
              NEWS
            </Animated.Text>
            <Animated.Text
              style={[
                styles.title,
                {transform: [{translateY: this._moveText}]},
              ]}>
              ỨNG DỤNG TIN TỨC
            </Animated.Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bg_image: {
    width: ScreenWidth,
    height: ScreenHeight,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: normalize(130),
    height: normalize(117),
    marginBottom: getHeight(35),
    resizeMode: 'contain',
  },
  title: {
    fontSize: getHeight(16),
    color: '#fff',
    fontFamily: fontFamily.f2,
    marginBottom: getHeight(5),
  },
});

export default Splash;
