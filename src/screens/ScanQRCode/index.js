import React, {Component} from 'react';
import {Text, View, Alert} from 'react-native';
import {RNCamera} from 'react-native-camera';
export class ScanQRCode extends Component {
  scanQRcode = e => {
    try {
      alert(e.data);
    } catch (e) {
      alert(e);
    }
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <RNCamera
          captureAudio={false}
          ref={ref => {
            this.camera = ref;
          }}
          style={{flex: 1}}
          onBarCodeRead={this.scanQRcode}
        />
      </View>
    );
  }
}

export default ScanQRCode;
