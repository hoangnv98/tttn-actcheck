import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  RefreshControl,
} from 'react-native';
import {Header, EventFeatured} from '@components';
import {Images} from '@config';
import styles from './styles';
import {Style, Colors} from '@common';
import data from './data';
export class Event extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isRefresing: false,
    };
  }

  onRefresh = () => {
    this.setState({isRefresing: !this.state.isRefresing});
  };
  renderItem = () => {
    let listItem = data.map((item, i) => {
      return <EventFeatured />;
    });
    return listItem;
  };

  render() {
    return (
      <View style={Style.body}>
        <Header
          left={'logo'}
          // right={'notify'}
          title={'Tin tức'}
          bottomBorder={true}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              tintColor={Colors.c1}
              onRefresh={this.onRefresh}
              refreshing={this.state.isRefresing}
            />
          }>
          <View>
            <Text style={styles.head}>Tin tức nổi bật</Text>
            <ScrollView
              style={styles.featuredContainer}
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {this.renderItem()}
            </ScrollView>
          </View>
          <View>
            <Text style={[styles.head, styles.mt_10]}>Danh sách tin tức</Text>
            <View style={styles.contentContainer}>
              <TouchableOpacity style={[styles.row, styles.item]}>
                <View style={styles.imageView}>
                  <Image source={Images.img_ev_1} style={styles.image} />
                </View>
                <View style={styles.content}>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode={'tail'}
                    style={styles.title}>
                    Kiểm tra việc hỗ trợ người dân gặp khó khăn do dịch Covid-19
                  </Text>
                  <View style={[styles.row, styles.mb_10]}>
                    <Image style={styles.icon} source={Images.ic_calendar} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      20/05/2020 09:03 AM
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Image style={styles.icon} source={Images.ic_answer} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      Kiểm tra tình hình dịch bệnh ...
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.row, styles.item]}>
                <View style={styles.imageView}>
                  <Image source={Images.img_ev_2} style={styles.image} />
                </View>
                <View style={styles.content}>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode={'tail'}
                    style={styles.title}>
                    Xét xử trùm giang hồ cướp và tẩu tán gỗ lậu
                  </Text>
                  <View style={[styles.row, styles.mb_10]}>
                    <Image style={styles.icon} source={Images.ic_calendar} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      23/05/2020 10:05 AM
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Image style={styles.icon} source={Images.ic_answer} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      Ngày 21-5, TAND tỉnh Gia Lai xét xử đối tượng Nguyễn Mạnh
                      Hùng (biệt danh Hùng “si đa”), 37 tuổi, ở phường Thống
                      Nhất, TP Pleiku
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.row, styles.item]}>
                <View style={styles.imageView}>
                  <Image source={Images.img_ev_3} style={styles.image} />
                </View>
                <View style={styles.content}>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode={'tail'}
                    style={styles.title}>
                    Mỹ kêu gọi Palestine tiếp tục hợp tác an ninh với Israel
                  </Text>
                  <View style={[styles.row, styles.mb_10]}>
                    <Image style={styles.icon} source={Images.ic_calendar} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      23/05/2020 10:00 AM
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Image style={styles.icon} source={Images.ic_answer} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      Ngày 20-5, Ngoại trưởng Mỹ Mike Pompeo bày tỏ hy vọng
                      Israel và Palestine vẫn sẽ duy trì hợp tác, sau khi Tổng
                      thống Palestine Mahmoud Abbas tuyên bố Palestine không
                      tiép tục thực hiện các cam kế trong thỏa thuận đã ký với
                      Mỹ và Israel.
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Event;
