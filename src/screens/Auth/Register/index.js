import React, {Component} from 'react';
import {Text, Image, View, TouchableOpacity} from 'react-native';
import {InputCustom, Header} from '@components';
import {Images} from '@config';
import {Style} from '@common';
import styles from '../styles';
import stylesRegister from './styles';

export class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPass: true,
      layoutHeight: 0,
    };
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    this.setState({contentHeight: contentHeight});
  };

  onLayout = evt => {
    this.setState({layoutHeight: evt.nativeEvent.layout.height});
  };
  onPressRegister = () => {
    this.props.navigation.navigate('Login');
  };

  render() {
    return (
      <View style={[Style.body, styles.body]}>
        <View style={styles.coverView}>
          <Image style={styles.cover} source={Images.bg_login} />
        </View>
        <Header
          left={'back'}
          backgroundColor={'transparent'}
          barStyle={'light-content'}
        />
        <Image
          source={Images.logo}
          style={[styles.logo, stylesRegister.logo]}
        />
        <View style={[styles.box, Style.boxShadow, stylesRegister.box]}>
          <Text style={styles.title}>Đăng ký thành viên</Text>
          <View style={stylesRegister.form}>
            <InputCustom
              placeholder={'Họ và tên'}
              prependType={'icon'}
              iconPrepend={Images.ic_mail}
              keyboardType={'email-address'}
              styleGroup={stylesRegister.inputGroup}
            />
            <InputCustom
              placeholder={'Tên tài khoản'}
              prependType={'icon'}
              iconPrepend={Images.ic_mail}
              styleGroup={stylesRegister.inputGroup}
            />
            <InputCustom
              placeholder={'Số điện thoại'}
              prependType={'icon'}
              iconPrepend={Images.ic_phone}
              styleGroup={stylesRegister.inputGroup}
            />
            <InputCustom
              placeholder={'Mật khẩu'}
              secureTextEntry={true}
              prependType={'icon'}
              iconPrepend={Images.ic_password}
              styleGroup={stylesRegister.inputGroup}
            />
            <InputCustom
              placeholder={'Nhập lại mật khẩu'}
              secureTextEntry={true}
              prependType={'icon'}
              iconPrepend={Images.ic_repassword}
              styleGroup={stylesRegister.inputGroup}
            />
          </View>
          <TouchableOpacity style={styles.btn} onPress={this.onPressRegister}>
            <Text style={styles.btnTitle}>Đăng ký</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Register;
