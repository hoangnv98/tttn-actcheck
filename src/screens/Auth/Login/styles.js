import {StyleSheet} from 'react-native';
import {getHeight, Colors} from '@common';

const styles = StyleSheet.create({
  form: {
    paddingVertical: getHeight(80),
  },
  buttonForgot: {
    alignSelf: 'flex-end',
  },
  titleForgot: {
    color: Colors.c2,
    fontSize: getHeight(12),
  },
  suggestTitle: {
    color: '#222222',
    fontSize: getHeight(14),
    marginRight: getHeight(5),
  },
  registerTitle: {
    fontSize: getHeight(14),
    color: Colors.c2,
  },
  suggest: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: getHeight(25),
  },
});

export default styles;
