import React, {Component} from 'react';
import {Text, Image, View, ScrollView, TouchableOpacity} from 'react-native';
import {InputCustom, Header} from '@components';
import {Images} from '@config';
import {Style} from '@common';
import styles from '../styles';
import stylesLogin from './styles';

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPass: true,
      layoutHeight: 0,
    };
    this.onShowPassPress = this.onShowPassPress.bind(this);
    this.onPressLogin = this.onPressLogin.bind(this);
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    this.setState({contentHeight: contentHeight});
  };

  onLayout = evt => {
    this.setState({layoutHeight: evt.nativeEvent.layout.height});
  };

  onShowPassPress = () => {
    this.setState({showPass: !this.state.showPass});
  };

  onRegisterPress = () => {
    this.props.navigation.navigate('Register');
  };

  onForgotPassPress = () => {
    this.props.navigation.navigate('Email');
  };

  onPressLogin = () => {
    this.props.navigation.navigate('TabStack');
  };

  render() {
    return (
      <View style={[Style.body, styles.body]}>
        <View style={styles.coverView}>
          <Image style={styles.cover} source={Images.bg_login} />
        </View>
        <ScrollView
          onLayout={this.onLayout}
          onContentSizeChange={this.onContentSizeChange}
          scrollEnabled={this.state.contentHeight > this.state.layoutHeight}
          style={styles.container}
          keyboardDismissMode={'on-drag'}>
          <Header backgroundColor={'transparent'} barStyle={'light-content'} />
          <Image source={Images.logo} style={styles.logo} />
          <View style={[styles.box, Style.boxShadow]}>
            <Text style={styles.title}>Đăng nhập</Text>
            <View style={stylesLogin.form}>
              <InputCustom
                placeholder={'Tên tài khoản'}
                prependType={'icon'}
                iconPrepend={Images.ic_mail}
                keyboardType={'email-address'}
              />
              <InputCustom
                placeholder={'Mật khẩu'}
                secureTextEntry={this.state.showPass}
                prependType={'icon'}
                iconPrepend={Images.ic_password}
                appendType={'icon'}
                iconAppend={
                  this.state.showPass
                    ? Images.ic_show_pass
                    : Images.ic_not_show_pass
                }
                onAppendPress={this.onShowPassPress}
              />
            </View>
            <TouchableOpacity onPress={this.onPressLogin} style={styles.btn}>
              <Text style={styles.btnTitle}>Đăng nhập</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={stylesLogin.suggest}
            onPress={this.onRegisterPress}>
            <Text style={stylesLogin.suggestTitle}>
              Chưa có tài khoản Hội viên?
            </Text>
            <Text style={stylesLogin.registerTitle}>Đăng ký</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

export default Login;
