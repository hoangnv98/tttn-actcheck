import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image, ScrollView} from 'react-native';
import {Header, EventFeatured} from '@components';
import {Images} from '@config';
import styles from './styles';
import {Style} from '@common';
export class InfoUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isRefresing: false,
    };
  }

  onRefresh = () => {
    this.setState({isRefresing: !this.state.isRefresing});
  };
  //   renderItem = () => {
  //     let listItem = data.map((item, i) => {
  //       return <EventFeatured />;
  //     });
  //     return listItem;
  //   };

  render() {
    return (
      <View style={Style.body}>
        <Header
          left={'logo'}
          // right={'notify'}
          title={'Thông tin cá nhân'}
          bottomBorder={true}
        />
        <ScrollView>
          <View>
            <Text style={styles.head}>Thông tin cá nhân</Text>
            <ScrollView style={styles.content}>
              <EventFeatured />
            </ScrollView>
          </View>
          <View>
            <Text style={[styles.head, styles.mt_10]}>
              Danh sách sự kiện đã tham gia
            </Text>
            <View style={styles.contentContainer}>
              <TouchableOpacity style={[styles.row, styles.item]}>
                <View style={styles.imageView}>
                  <Image source={Images.img_list_ev_1} style={styles.image} />
                </View>
                <View style={styles.content}>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode={'tail'}
                    style={styles.title}>
                    LỄ KÝ NIỆM 20 NĂM THÀNH LẬP KHOA CÔNG NGHỆ THÔNG TIN -
                    TRƯỜNG ĐẠI HỌC CÔNG NGHIỆP HÀ NỘI
                  </Text>
                  <View style={[styles.row, styles.mb_10]}>
                    <Image style={styles.icon} source={Images.ic_calendar} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      20/05/2020 09:03 AM
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Image style={styles.icon} source={Images.ic_place} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      A11 - DHCNHN
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.row, styles.item]}>
                <View style={styles.imageView}>
                  <Image source={Images.img_list_ev_2} style={styles.image} />
                </View>
                <View style={styles.content}>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode={'tail'}
                    style={styles.title}>
                    Trao giải thưởng Miss HAUI 2020
                  </Text>
                  <View style={[styles.row, styles.mb_10]}>
                    <Image style={styles.icon} source={Images.ic_calendar} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      29/05/2020 08:10 PM
                    </Text>
                  </View>
                  <View style={styles.row}>
                    <Image style={styles.icon} source={Images.ic_place} />
                    <Text
                      numberOfLines={1}
                      ellipsizeMode={'tail'}
                      style={styles.info}>
                      A1 - DHCNHN
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default InfoUser;
