import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image, ScrollView} from 'react-native';
import {Header, EventFeatured, InputCustom} from '@components';
import {Images} from '@config';
import styles from './styles';
import {Style} from '@common';
export class RegisterEvent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isRefresing: false,
    };
    this.onPressRegisterEvent = this.onPressRegisterEvent.bind(this);
  }

  onRefresh = () => {
    this.setState({isRefresing: !this.state.isRefresing});
  };
  onPressRegisterEvent = () => {
    this.props.navigation.navigate('ScanQRCode');
  };
  //   renderItem = () => {
  //     let listItem = data.map((item, i) => {
  //       return <EventFeatured />;
  //     });
  //     return listItem;
  //   };

  render() {
    return (
      <View style={Style.body}>
        <Header
          left={'logo'}
          // right={'notify'}
          title={'Đăng ký sự kiện'}
          bottomBorder={true}
        />
        <ScrollView>
          <View>
            <Text style={styles.head}>Thông tin cá nhân</Text>
            <ScrollView style={styles.content}>
              <EventFeatured />
            </ScrollView>
          </View>
          <View>
            <Text style={[styles.head, styles.mt_10]}>
              Đăng ký tham gia sự kiện
            </Text>
          </View>
          <View style={styles.form}>
            <InputCustom
              placeholder={'Mã sự kiện'}
              prependType={'icon'}
              iconPrepend={Images.ic_mail}
              keyboardType={'email-address'}
            />
          </View>
          <TouchableOpacity
            style={styles.btn}
            onPress={this.onPressRegisterEvent}>
            <Text style={styles.btnTitle}>Đăng ký sự kiện</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

export default RegisterEvent;
