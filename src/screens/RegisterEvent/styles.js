import {StyleSheet} from 'react-native';
import {getWidth, getHeight, fontFamily, Colors} from '@common';
const styles = StyleSheet.create({
  featuredContainer: {
    paddingHorizontal: getWidth(6),
    paddingBottom: getHeight(20),
    overflow: 'visible',
  },
  row: {
    flexWrap: 'nowrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentScroll: {
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingHorizontal: getWidth(16),
  },
  head: {
    fontSize: getHeight(16),
    fontFamily: fontFamily.f2,
    color: '#222222',
    marginLeft: getWidth(16),
    marginTop: getHeight(25),
    marginBottom: getHeight(18),
  },
  item: {
    marginBottom: getHeight(20),
  },
  imageView: {
    width: getHeight(114.11),
    height: getHeight(113.99),
    overflow: 'hidden',
    borderRadius: getHeight(4),
    marginRight: getWidth(13),
  },
  image: {
    width: getWidth(114.11),
    height: getHeight(113.99),
    resizeMode: 'contain',
  },
  content: {
    flex: 1,
    flexGrow: 1,
  },
  btn: {
    backgroundColor: Colors.c2,
    borderRadius: getHeight(6.67),
    paddingVertical: getHeight(19),
  },
  btnTitle: {
    textTransform: 'uppercase',
    color: '#fff',
    textAlign: 'center',
    fontFamily: fontFamily.f3,
    fontSize: getHeight(14),
  },
  title: {
    fontFamily: fontFamily.f3,
    fontSize: getHeight(15),
    color: Colors.c4,
    marginBottom: getHeight(15),
  },
  icon: {
    width: getWidth(9.64),
    height: getHeight(11.57),
    resizeMode: 'contain',
  },
  info: {
    fontSize: getHeight(12),
    color: '#71716f',
    marginLeft: getWidth(5),
  },
  mb_10: {
    marginBottom: getHeight(10),
  },
  mt_10: {
    marginTop: getHeight(10),
  },
  form: {
    paddingVertical: getHeight(80),
  },
});

export default styles;
