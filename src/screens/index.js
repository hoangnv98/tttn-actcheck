import Splash from './Splash';
import Login from './Auth/Login';
import Register from './Auth/Register';
import Event from './Event';
import Detail from './Detail';
import InfoUser from './InfoUser';
import RegisterEvent from './RegisterEvent';
import ScanQRCode from './ScanQRCode';
export {
  Splash,
  Login,
  Register,
  Event,
  Detail,
  InfoUser,
  RegisterEvent,
  ScanQRCode,
};
