import {StyleSheet} from 'react-native';
import {ScreenWidth, getHeight, getWidth, fontFamily, Colors} from '@common';

const styles = StyleSheet.create({
  body: {
    backgroundColor: '#fff',
    flex: 1,
  },
  coverView: {
    position: 'absolute',
    zIndex: -1,
    top: 0,
    height: getHeight(266.66),
    overflow: 'hidden',
  },
  cover: {
    height: getHeight(266.66),
    width: ScreenWidth,
    resizeMode: 'cover',
  },
  container: {
    flex: 1,
    paddingBottom: getHeight(20),
  },
  logo: {
    marginTop: getHeight(9),
    width: getHeight(90),
    height: getHeight(81),
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  box: {
    marginTop: getHeight(40),
    backgroundColor: '#fff',
    marginHorizontal: getWidth(16),
    borderRadius: getHeight(10),
    paddingBottom: getHeight(27),
    paddingTop: getHeight(30),
    paddingHorizontal: getHeight(22),
  },
  title: {
    fontSize: getHeight(16),
    color: Colors.c1,
    textAlign: 'center',
    textTransform: 'uppercase',
    fontFamily: fontFamily.f2,
  },
  desc: {
    paddingVertical: getHeight(30),
    fontSize: getHeight(14),
    color: '#222222',
  },
  btn: {
    backgroundColor: Colors.c2,
    borderRadius: getHeight(6.67),
    paddingVertical: getHeight(13),
  },
  btnTitle: {
    textTransform: 'uppercase',
    color: '#fff',
    textAlign: 'center',
    fontFamily: fontFamily.f3,
    fontSize: getHeight(14),
  },
  textOption: {
    textAlign: 'center',
    paddingVertical: getHeight(25),
    fontSize: getHeight(12),
  },
  social: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: getHeight(5),
    paddingHorizontal: getHeight(6),
    borderRadius: getHeight(16.67),
    borderColor: '#6f6f6f',
    borderWidth: getHeight(0.33),
    marginHorizontal: getWidth(5),
  },
  iconSocial: {
    width: getHeight(24),
    height: getHeight(24),
    resizeMode: 'contain',
  },
  titleSocial: {
    fontSize: getHeight(12),
    minWidth: getWidth(62.63),
    color: '#222222',
    textAlign: 'center',
  },
  webView: {
    marginTop: getWidth(15),
  },
});

export default styles;
