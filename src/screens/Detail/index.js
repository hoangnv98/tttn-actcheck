import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import styles from './styles';
export class Detail extends Component {
  render() {
    return (
      <WebView
        style={styles.webView}
        originWhitelist={['*']}
        source={{
          uri: 'https://dev.apecsoft.asia/cktc/webview/news/detail/40',
        }}
      />
    );
  }
}
export default Detail;
