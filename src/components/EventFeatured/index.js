import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Images} from '@config';
import {getWidth, getHeight, fontFamily} from '@common';
import {useNavigation} from '@react-navigation/native';
export class EventFeatured extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPass: true,
      layoutHeight: 0,
    };
    this.openArticleDetailsScreen = this.openArticleDetailsScreen.bind(this);
  }
  // componentDidMount() {
  //   console.log('');
  // }
  openArticleDetailsScreen = () => {
    this.props.navigation.navigate('Detail');
  };
  render() {
    return (
      <View style={[styles.item, this.props.style]}>
        <TouchableOpacity onPress={this.openArticleDetailsScreen}>
          <View style={styles.imageView}>
            <Image style={styles.image} source={Images.img_ev_1} />
          </View>
          <View style={styles.content}>
            <View>
              <Text style={styles.date}>22</Text>
              <Text style={styles.month}>05</Text>
            </View>
            <View style={styles.main}>
              <Text style={styles.title}>
                Kiểm tra việc hỗ trợ người dân gặp khó khăn do dịch Covid-19
              </Text>
              <Text style={styles.cate}>COVID - 19</Text>
              <View style={styles.placeView}>
                <Image style={styles.icon} source={Images.ic_answer} />
                <Text style={styles.place}>
                  Kiểm tra tình hình dịch bệnh...
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    marginHorizontal: getWidth(10),
    backgroundColor: '#fff',
    borderRadius: getHeight(4),
    width: getWidth(307.66),
  },
  imageView: {
    height: getHeight(145.66),
    width: getWidth(307.66),
    overflow: 'hidden',
    borderTopLeftRadius: getHeight(4),
    borderTopRightRadius: getHeight(4),
  },
  image: {
    height: getHeight(145.66),
    width: getWidth(307.66),
    resizeMode: 'cover',
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: getHeight(10),
    paddingHorizontal: getWidth(5),
  },
  main: {
    marginLeft: getWidth(10),
  },
  date: {
    fontSize: getHeight(21.32),
    color: '#00aaf5',
    textAlign: 'center',
    fontFamily: fontFamily.f3,
  },
  month: {
    fontSize: getHeight(13.34),
    color: '#00aaf5',
    textAlign: 'center',
  },
  title: {
    fontFamily: fontFamily.f3,
    fontSize: getHeight(14.67),
    color: '#3a3936',
    textTransform: 'uppercase',
  },
  cate: {
    fontSize: getHeight(13.34),
    color: '#3a3936',
    opacity: 0.8,
    textTransform: 'uppercase',
  },
  placeView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: getWidth(8),
    height: getHeight(11.22),
    resizeMode: 'contain',
  },
  place: {
    fontSize: getHeight(13.34),
    color: '#3a3936',
    opacity: 0.8,
    marginLeft: getWidth(5),
  },
});

export default function(props) {
  const navigation = useNavigation();

  return <EventFeatured {...props} navigation={navigation} />;
}
