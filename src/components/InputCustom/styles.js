import {StyleSheet} from 'react-native';
import {getHeight, getWidth, Colors, fontFamily} from '@common';

const styles = StyleSheet.create({
  formGroup: {
    position: 'relative',
    marginBottom: getHeight(30),
  },
  label: {
    fontSize: getHeight(9),
  },
  input: {
    paddingVertical: getHeight(5),
    paddingHorizontal: getWidth(25),
    borderBottomWidth: 1,
    borderBottomColor: '#e5e5e5',
    color: '#222222',
    fontSize: getHeight(12),
    fontFamily: fontFamily.f1,
  },
  placeholder: {
    fontSize: getHeight(12),
    fontFamily: fontFamily.f1,
  },
  prependView: {
    top: 0,
    left: 0,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: getWidth(25),
    height: '100%',
  },
  appendView: {
    top: 0,
    right: 0,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: getWidth(25),
    height: '100%',
  },
  icon: {
    width: getHeight(14),
    height: getHeight(14),
    resizeMode: 'contain',
  },
});

export default styles;
