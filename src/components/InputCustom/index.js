import {View, TextInput, Image, Text, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import styles from './styles';

export default class InputCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clear: this.props.value !== '' ? true : false,
      value: '',
    };
    // this.onClearText = this.onClearText.bind(this)
  }
  onChangeText = value => {
    this.setState({
      value: value,
      clear: value !== '' ? true : false,
    });
    this.props.onChangeText(value);
  };
  onClearText() {
    this.onChangeText('');
  }

  renderPrepend = () => {
    if (this.props.prependType === 'icon') {
      return (
        <TouchableOpacity
          style={styles.prependView}
          onPress={this.props.onPrependPress}>
          <Image style={styles.icon} source={this.props.iconPrepend} />
        </TouchableOpacity>
      );
    }
  };

  renderAppend = () => {
    if (this.props.appendType === 'icon') {
      return (
        <TouchableOpacity
          style={styles.appendView}
          onPress={this.props.onAppendPress}>
          <Image style={styles.icon} source={this.props.iconAppend} />
        </TouchableOpacity>
      );
    }
  };

  renderLabel() {
    return this.props.label ? (
      <Text style={styles.label}>{this.props.label}</Text>
    ) : null;
  }

  _onChangeText(text) {
    if (this.props.onChangeText) {
      this.props.onChangeText && this.props.onChangeText(text);
    }
  }

  render() {
    return (
      <>
        {this.renderLabel()}
        <View style={[styles.formGroup, this.props.styleGroup]}>
          {this.renderPrepend()}
          <TextInput
            style={[styles.input, this.props.styleInput]}
            onChangeText={value => this._onChangeText(value)}
            placeholderStyle={styles.placeholder}
            placeholderTextColor={'rgba(34, 34, 34, 0.6)'}
            {...this.props}
          />
          {this.renderAppend()}
        </View>
      </>
    );
  }
}
