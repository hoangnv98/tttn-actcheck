import React, {Component, useState, useEffect} from 'react';
import {Text, View, Image, TouchableOpacity, StatusBar} from 'react-native';
import styles from './styles';
import {Images} from '@config';
import {useNavigation} from '@react-navigation/native';

function Notify(props) {
  let [notify, setNotify] = useState(true);
  useEffect(() => {
    setNotify(props.hasNotify);
  }, [props.hasNotify]);

  return (
    <View style={styles.notifyView}>
      <Image style={styles.notify} source={Images.ic_notify} />
      {notify ? <View style={styles.hasNotify} /> : null}
    </View>
  );
}

export class Header extends Component {
  onLeftPress = () => {
    if (this.props.left === 'back' || this.props.left === 'close') {
      this.props.navigation.goBack();
    } else {
      this.props.onLeftPress;
    }
  };

  onRightPress = () => {
    if (this.props.right === 'notify') {
      this.props.navigation.navigate('Notification');
    } else if (this.props.right === 'history') {
      this.props.navigation.navigate('History');
    } else {
      this.props.onRightPress;
    }
  };

  renderLeft = () => {
    if (this.props.left === 'logo') {
      return <Image style={styles.logo} source={Images.logo_home} />;
    }
    if (this.props.left === 'back') {
      return (
        <Image
          style={[
            styles.back,
            this.props.backgroundColor === 'transparent'
              ? // eslint-disable-next-line react-native/no-inline-styles
                {tintColor: '#fff'}
              : null,
          ]}
          source={Images.ic_back_dark}
        />
      );
    }
    if (this.props.left === 'close') {
      return <Image style={styles.back} source={Images.ic_close} />;
    }
  };

  renderCenter = () => {
    if (this.props.renderCenter) {
      return this.props.renderCenter;
    }
    return <Text style={styles.title}>{this.props.title}</Text>;
  };

  renderRight = () => {
    if (this.props.right === 'notify') {
      return <Notify hasNotify={this.props.hasNotify} />;
    }
    if (this.props.right === 'answer') {
      return <Text style={styles.answer}>Trả lời</Text>;
    }
    if (this.props.right === 'send') {
      return <Text style={styles.answer}>Gửi</Text>;
    }
    if (this.props.right === 'history') {
      return <Text style={styles.history}>Lịch sử đóng phí</Text>;
    }
  };

  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.backgroundColor
            ? {backgroundColor: this.props.backgroundColor}
            : null,
        ]}>
        <StatusBar
          barStyle={this.props.barStyle ? this.props.barStyle : 'dark-content'}
          backgroundColor={'transparent'}
          translucent={true}
          animated={true}
        />
        <View
          style={[
            styles.header,
            this.props.bottomBorder ? styles.borderHeader : null,
          ]}>
          <View style={styles.content}>
            <TouchableOpacity
              style={[styles.left, this.props.styleLeft]}
              onPress={() => this.onLeftPress()}>
              {this.renderLeft()}
            </TouchableOpacity>
            <TouchableOpacity style={[styles.center, this.props.styleCenter]}>
              {this.renderCenter()}
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.right, this.props.styleRight]}
              onPress={() => this.onRightPress()}>
              {this.renderRight()}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();
  return <Header {...props} navigation={navigation} />;
}
