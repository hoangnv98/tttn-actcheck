import {StyleSheet} from 'react-native';
import {
  getHeight,
  getWidth,
  fontFamily,
  Colors,
  getStatusBarHeight,
} from '@common';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    backgroundColor: '#fff',
    height: getHeight(44) + getStatusBarHeight(),
  },
  header: {
    position: 'absolute',
    top: getStatusBarHeight(),
    left: 0,
    zIndex: 1,
    width: '100%',
    height: getHeight(44),
  },
  borderHeader: {
    borderBottomColor: '#eeeeee',
    borderBottomWidth: 1,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: getHeight(5),
  },
  left: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    zIndex: 3,
    justifyContent: 'center',
  },
  logo: {
    resizeMode: 'contain',
    height: getHeight(32),
    width: getHeight(32),
    marginLeft: getWidth(18),
  },
  back: {
    width: getWidth(14.99),
    height: getHeight(19),
    resizeMode: 'contain',
    marginHorizontal: getWidth(9),
  },
  center: {
    flex: 1,
    flexGrow: 1,
  },
  title: {
    textAlign: 'center',
    fontFamily: fontFamily.f2,
    fontSize: getHeight(17),
    color: '#3a3936',
  },
  right: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    zIndex: 3,
    justifyContent: 'center',
  },
  notifyView: {
    marginRight: getWidth(18),
  },
  notify: {
    height: getHeight(18),
    width: getWidth(20),
    resizeMode: 'contain',
  },
  hasNotify: {
    position: 'absolute',
    height: getHeight(6),
    width: getHeight(6),
    backgroundColor: '#ff3b01',
    right: 1,
    top: 0,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 3,
  },
  answer: {
    textTransform: 'uppercase',
    fontFamily: fontFamily.f3,
    fontSize: getHeight(16.67),
    marginRight: getWidth(16),
    color: Colors.c2,
  },
  history: {
    textTransform: 'uppercase',
    fontFamily: fontFamily.f3,
    fontSize: getHeight(14),
    marginRight: getWidth(16),
    color: '#fff',
  },
});

export default styles;
