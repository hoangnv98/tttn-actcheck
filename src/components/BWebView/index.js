import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {WebView} from 'react-native-webview';
import Toast from 'react-native-simple-toast';
import {
  ScreenHeight,
  ScreenWidth,
  openURL,
  openPhoneCall,
  openEmailApp,
} from '@common';
import {Config} from '@config';

export class BWebView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  //switch interface
  switchInterface = (val) => {
    switch (val.act) {
      case 'logout':
        this.logout();
        break;
      case 'openURL':
        openURL(val.url);
        break;
      case 'call':
        openPhoneCall(val.phone);
        break;
      case 'email':
        openEmailApp(val.to);
        break;
      case 'cusHead':
        this.showDetail(val);
        break;
      case 'toast':
        Toast.show(val.content);
        break;
    }
  };

  showDetail = (val) => {};

  onReload = () => {
    let link = this.props.uri;
    link =
      link.indexOf('?') >= 0
        ? link + '&vti=' + new Date().getTime()
        : link + '?vti=' + new Date().getTime();
    this.setState({url: link});
  };

  render() {
    // let scriptImport = `
    //     var imported = document.createElement('script');
    //     imported.src = '${Config.url_js}?time=${Date.now()}';
    //     document.head.appendChild(imported);true;`;
    let headersWeb = {
      'x-token': Config.token,
      'lang-code': Config.lang_code,
    };
    return (
      <WebView
        style={{width: ScreenWidth, height: ScreenHeight}}
        originWhitelist={['*']}
        showsVerticalScrollIndicator={false}
        source={{uri: this.props.url, headers: headersWeb}}
        //injectedJavaScript={scriptImport}
        javaScriptEnabled={true}
        onError={(e) => {
          __DEV__ && console.log('load error...' + this.props.url);
        }}
        onMessage={(event) => {
          this.switchInterface(JSON.parse(event.nativeEvent.data));
        }}
        onLoadEnd={() => {
          this.setState({isLoading: false});
          __DEV__ && console.log('load end...' + new Date());
          __DEV__ && console.log('load end...' + this.props.url);
        }}
        onLoadStart={() => {
          this.setState({isLoading: true});
          __DEV__ && console.log('load start..' + new Date());
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default BWebView;
