import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {getHeight, Colors} from '@common';

export default class Radio extends Component {
  render() {
    if (this.props.isChecked && this.props.isChecked === true) {
      return (
        <View style={[styles.radio, styles.radioParent]}>
          <View style={[styles.radioChild]} />
        </View>
      );
    } else {
      return <View style={[styles.radio, styles.notChecked]} />;
    }
  }
}

const styles = StyleSheet.create({
  radio: {
    width: getHeight(20),
    height: getHeight(20),
    borderRadius: getHeight(10),
    borderWidth: getHeight(1),
    padding: getHeight(2),
  },
  notChecked: {
    borderColor: '#c3c3c3',
  },
  radioChild: {
    width: getHeight(14),
    height: getHeight(14),
    borderRadius: getHeight(14),
    backgroundColor: Colors.c2,
  },
  radioParent: {
    borderColor: Colors.c2,
  },
});
