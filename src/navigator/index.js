import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Splash,
  Login,
  Register,
  Event,
  Detail,
  InfoUser,
  RegisterEvent,
  ScanQRCode,
} from '@screens';
import {Colors} from '@common';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Images} from '@config';
import {Style} from '@common';
const TabStack = createBottomTabNavigator();

const TabStackScreen = () => (
  <TabStack.Navigator
    initialRouteName={'Home'}
    swipeEnabled={true}
    lazy={true}
    tabBarPosition={'bottom'}
    tabBarOptions={{
      showIcon: false,
      style: {
        backgroundColor: Colors.c1,
      },
      activeTintColor: Colors.c2,
      inactiveTintColor: '#fff',
    }}>
    <TabStack.Screen
      name={'Event'}
      component={Event}
      options={{
        title: 'Thông tin bài viết',
      }}
    />
    {/* <TabStack.Screen
      name={'RegisterEvent'}
      component={RegisterEvent}
      options={{
        title: 'Đăng ký sự kiện',
      }}
    />
    <TabStack.Screen
      name={'ScanQRCode'}
      component={ScanQRCode}
      options={{
        title: 'Quét mã',
      }}
    />
    <TabStack.Screen
      name={'InfoUser'}
      component={InfoUser}
      options={{
        title: 'Cá nhân',
        tabBarIcon: () => (
          <Image
            source={Images.ic_qrcode}
            style={[Style.iconTabBottom, {tintColor: '#fff'}]}
          />
        ),
      }}
    /> */}
  </TabStack.Navigator>
);

const MainStack = createStackNavigator();

const MainStackScreen = () => (
  <MainStack.Navigator initialRouteName={'Login'} headerMode={'none'}>
    <MainStack.Screen name={'TabStack'} component={TabStackScreen} />
    <MainStack.Screen name={'Login'} component={Login} />
    <MainStack.Screen name={'Register'} component={Register} />
    <MainStack.Screen name={'Detail'} component={Detail} />
    <MainStack.Screen name={'InfoUser'} component={InfoUser} />
    <MainStack.Screen name={'RegisterEvent'} component={RegisterEvent} />
  </MainStack.Navigator>
);

const Stack = createStackNavigator();
export default () => (
  <NavigationContainer>
    <Stack.Navigator initialRouteName={'Splash'} headerMode={'none'}>
      <Stack.Screen name={'Splash'} component={Splash} />
      <Stack.Screen name={'MainStack'} component={MainStackScreen} />
    </Stack.Navigator>
  </NavigationContainer>
);
