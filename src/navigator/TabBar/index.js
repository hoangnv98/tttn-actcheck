import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Colors, getHeight, getWidth, hasNotch, fontFamily} from '@common';

export default function TabBar({
  state,
  descriptors,
  navigation,
  activeTintColor,
  inactiveTintColor,
}) {
  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.tab}>
            {options.tabBarIcon({
              color: isFocused ? activeTintColor : inactiveTintColor,
            })}
            <Text
              style={[
                styles.title,
                {color: isFocused ? activeTintColor : inactiveTintColor},
              ]}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.c1,
    justifyContent: 'space-between',
    paddingTop: getHeight(9),
    paddingHorizontal: getWidth(13),
    paddingBottom: hasNotch() ? getHeight(27) : getHeight(9),
  },
  tab: {
    minWidth: getWidth(49.22),
    alignItems: 'center',
  },
  title: {
    marginTop: getHeight(8),
    fontFamily: fontFamily.f6,
    fontSize: getHeight(9.9),
  },
});
