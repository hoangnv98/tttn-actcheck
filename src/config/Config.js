export const Config = {
  fcm_token: '',
  token: '',
  timeout: 120000,
  // base_url: 'https://dev.apecsoft.asia/cktc/',
  // base_url_api: 'https://dev.apecsoft.asia/cktc/api',
  url_js: '',
  access_token: null,
  is_login: 0,
  is_notify: 1,
  lang_code: 'vi',
  api_youtube: 'AIzaSyB8WfsZ-MoYqGSK5IIrIH4xMrGiLMWLyTA',
  //Key lưu async storage
  storage: {
    base_url: 'base_url',
    base_url_api: 'base_url_api',
    access_token: 'access_token',
    fcm_token: 'fcm_token',
    is_login: 'is_login',
    is_notify: 'is_notify',
    lang_code: 'lang_code',
  },
};
