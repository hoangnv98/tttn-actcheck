export const Images = {
  //Icon
  ic_password: require('@assets/images/ic_password.png'),
  ic_repassword: require('@assets/images/ic_repassword.png'),
  ic_show_pass: require('@assets/images/ic_show_pass.png'),
  ic_not_show_pass: require('@assets/images/ic_not_show_pass.png'),
  ic_mail: require('@assets/images/ic_mail.png'),
  ic_phone: require('@assets/images/ic_phone.png'),
  ic_back_dark: require('@assets/images/ic_back_dark.png'),
  ic_place: require('@assets/images/ic_place.png'),
  ic_calendar: require('@assets/images/ic_calendar.png'),
  ic_qrcode: require('@assets/images/ic_qrcode.png'),
  ic_answer: require('@assets/images/ic_answer.png'),
  //Default
  //logo: require('@assets/images/logo.png'),
  logo: require('@assets/images/logo.png'),
  logo_home: require('@assets/images/logo_home.png'),
  bg_splash: require('@assets/images/bg_splash.jpg'),
  bg_login: require('@assets/images/bg_login.jpg'),
  //Demo
  img_ev_1: require('@assets/images/event/ev_1.jpg'),
  img_ev_2: require('@assets/images/event/ev_2.jpg'),
  img_ev_3: require('@assets/images/event/ev_3.jpg'),
  img_home_list1: require('@assets/images/event/img_home_list1.png'),
  // Content
  img_list_ev_1: require('@assets/images/event/fit.png'),
  img_list_ev_2: require('@assets/images/event/fit-haui.jpg'),
  //AVARTAR
  img_avatar: require('@assets/images/event/user_1.jpg'),
};
