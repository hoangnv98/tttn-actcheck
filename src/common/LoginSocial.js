import {GoogleSignin} from '@react-native-community/google-signin';

import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

export const loginGoogle = async (callback) => {
  try {
    await GoogleSignin.hasPlayServices({
      showPlayServicesUpdateDialog: true,
    });
    await GoogleSignin.configure({
      iosClientId:
        '759404087224-is6jjmpkbno1fedh4ah2bekl9jg96979.apps.googleusercontent.com',
    });
    let result = await GoogleSignin.signIn();
    callback(result);
  } catch (error) {}
};

export const loginFacebook = async (callback) => {
  try {
    let result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);
    if (!result.isCancelled) {
      AccessToken.getCurrentAccessToken().then((data) => {
        const infoRequest = new GraphRequest(
          '/me?fields=name,first_name,last_name,picture,email',
          null,
          callback,
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      });
    }
  } catch (error) {}
};
