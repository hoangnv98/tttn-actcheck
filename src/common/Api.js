export const Api = {
  Auth: {
    guest_token: 'auth/getTokenGuest',
    login: 'auth/login',
    refresh: 'auth/refresh_token',
    initial_device: 'auth/initial_device',
  },
};
