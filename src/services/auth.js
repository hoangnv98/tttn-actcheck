import {Api, Request} from '@common';

export const auth = {
  guest_token: async () => {
    let jsonPost = {
      get_token_guest: true,
    };
    return await Request.post(Api.Auth.guest_token, jsonPost);
  },
  login: async (username, password) => {
    let jsonPost = {
      identity: username.trim(),
      password: password.trim(),
    };
    return await Request.post(Api.Auth.login, jsonPost);
  },
  initial_device: async (fcm_token, is_notify) => {
    let jsonPost = {
      fcm_token: fcm_token,
      is_notify: is_notify,
    };
    return await Request.post(Api.Auth.initial_device, jsonPost);
  },
  refresh: async () => {
    return await Request.get(Api.Auth.refresh);
  },
};
